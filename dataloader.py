import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
#import sklearn as sk


def load_android_data(directory):
    file_paths = []
    for (dirpath, dirnames, filenames) in os.walk(directory):
        if (len(filenames)>0):
            file_paths.append((dirpath,filenames))

    df = None
    for path,files in file_paths:
        for file in files:
            temp = pd.read_csv(os.path.join(path,file))
            if (df is not None):
                df = pd.concat([df,temp])
            else:
                df = temp
    return df

def from_tuple_list(data):
    df = pd.DataFrame(data)
    df.columns = ["x","y","z","MAC Address","RSSI","SSID","type"]
    return df


def load(directory):
    file_paths = []
    for (dirpath, dirnames, filenames) in os.walk(directory):
        if (len(filenames)>0):
            file_paths.append((dirpath,filenames))

    df = None
    for path,files in file_paths:
        for file in files:
            coords = file.split(".")[0].split("_")
            temp = pd.read_csv(os.path.join(path,file))
            temp.insert(0,"x",int(coords[1]))
            temp.insert(1,"y",int(coords[2]))
            if (df is not None):
                df = pd.concat([df,temp])
            else:
                df = temp
    return df

def filter(df:pd.DataFrame,indices):
    return df[indices]

def collect_macs(df,column_name="MAC Address"):
    return df[column_name].unique()
'''
def reduce(funct,arr):
    def helper_reduce(start,end):
        div = int((end - start)/2)
        if end-start<=1:
            return arr[start]
        x =  funct(helper_reduce(start,start+div),helper_reduce(start+div,end))
        if len(x)>200:
            print(x)
        return x
    return helper_reduce(0,len(arr))
'''


def gethash(x,y):
    return str(x)+","+str(y)

def get_vector_from_row(row):
    return row.iloc[2:].to_numpy()

def get_vector(df,x,y,mapping):
    return get_vector_from_row(df.iloc[mapping[gethash(x,y)]])

def to_list(df):
    return [(tuple(r.iloc[:2]),get_vector_from_row(r)) for _,r in df.iterrows()]

def transform(df,macs,mac_name="MAC Address",rssi_name="RSSI"):
    temp = df.copy()
    new_temp = temp[["x","y"]].drop_duplicates()
    print(list(new_temp.to_numpy()))

    def gethashrow(row):
        return gethash(row['x'],row['y'])

    mapper = {}
    for n,(_, row )in enumerate(new_temp.iterrows()):
        mapper[gethashrow(row)] = n

    columns = {
        "x":new_temp["x"].to_numpy(),
        "y":new_temp["y"].to_numpy()
        }
    for m in macs:
        things = np.zeros(len(mapper))
        tempmac = temp.loc[temp[mac_name]==m]
        for _,i in tempmac.iterrows():
            things[mapper[gethashrow(i)]] = i[rssi_name]
        columns[m]=things

    
    result = pd.DataFrame(columns)

    return result, mapper

# macs is obtained from collect_macs over the df
# input is a dictionary MAC : signal strength
def create_rssi_vector(input,macs):
    vector = np.zeros(len(macs))
    for n, i in enumerate(macs):
        if i in input:
            vector[n] = input[i]

    return vector

if __name__ == "__main__":
    df = load("data")
    filtered = filter(df,["x","y","MAC Address","RSSI","Signal Quality"])
    macs = collect_macs(filtered)
    print(len(set(macs)))
    transformed_df, mapping = transform(filtered,macs)
    print(transformed_df)
    print(get_vector(transformed_df,103,4,mapping))

