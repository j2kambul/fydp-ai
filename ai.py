import pandas as pd
import numpy as np



#distance is only based on things you know
#lacking information will not cause it to increase in distance
def dist(v1,v2):
    diff = np.array([x-y if x*y>0 else 0 for x,y in zip(v1,v2)])
    return np.linalg.norm(diff)

def make_dist_vect(target,data):
    return np.array([(pos[0],pos[1],dist(target,d)) for (pos,d) in data])


def interpolate(points):
    arr = 1/(points[:,2]+1e-3)
    return np.dot(points[:,:2].T,arr/sum(arr))
'''
# if you want to speed something up, speed this up
def bottomk(data,k):
    return data[np.argsort(data[:,2])][:k]

'''
def bottomk(data,k):
    arr = []

    def addtolist(x):
        current = x
        if len(arr)==k and x[2]>arr[-1][2]:
            return
        for i in range(len(arr)):
            if arr[i][2]>current[2]:
                temp = arr[i]
                arr[i] = current
                current = temp

        if (len(arr)<k):
            arr.append(current)


    for x in data:
        addtolist(x)

    return np.array(arr)
#'''

def project(x,map):
    return map[np.argmin(np.linalg.norm(map-x,axis=0))]


def inference(rssi_data,dataset,k=5):
    dist_vec = make_dist_vect(rssi_data,dataset)
    topk = bottomk(dist_vec,k)
    x,y = interpolate(topk)
    return x,y

def main():
    pass


if __name__ == "__main__":
    main()